package com.zuitt.batch193;

import java.util.Scanner;

public class LeapYear {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);
        //input year
        System.out.println("Input year to be checked if a leap year.");
        int isLeapYear = appScanner.nextInt();
        //if else statements for leap year
        if (isLeapYear % 4 == 0)
            System.out.println(isLeapYear + " is a leap year");
        else if (isLeapYear % 100 == 0 & isLeapYear % 400 == 0)
            System.out.println(isLeapYear + " is a leap year");
        else
            System.out.println(isLeapYear + " is NOT a leap year");
    }
}
